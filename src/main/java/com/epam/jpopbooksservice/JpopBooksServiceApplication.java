package com.epam.jpopbooksservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpopBooksServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpopBooksServiceApplication.class, args);
	}

}
