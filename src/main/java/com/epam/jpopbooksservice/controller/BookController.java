package com.epam.jpopbooksservice.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.jpopbooksservice.domain.Book;
import com.epam.jpopbooksservice.exception.BookNotFoundException;
import com.epam.jpopbooksservice.service.BooksService;

@RestController
public class BookController {

	@Autowired
	private BooksService bookService;

	@GetMapping("/books")
	public ResponseEntity<List<Book>> getAll() {
		return ResponseEntity.ok(bookService.getAll());
	}

	@GetMapping("/books/{book_id}")
	public ResponseEntity<Book> get(@PathVariable("book_id") long id) {
		return ResponseEntity.ok(checkBookAndReturn(id));
	}

	@PostMapping("/books")
	public ResponseEntity<Object> add(@RequestBody Book book) {
		Book savedBook = bookService.save(book);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedBook.getId())
				.toUri();
		return ResponseEntity.created(location).body(savedBook);
	}

	@DeleteMapping("/books/{book_id}")
	public ResponseEntity<Object> remove(@PathVariable("book_id") long id) {
		checkBookAndReturn(id);
		bookService.remove(id);
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/books/{book_id}")
	public ResponseEntity<Object> update(@RequestBody Book book, @PathVariable("book_id") long id) {
		checkBookAndReturn(id);
		book.setId(id);
		Book updatedBook = bookService.save(book);
		return ResponseEntity.status(204).build();
	}

	private Book checkBookAndReturn(long id) {
		Optional<Book> book = bookService.getById(id);
		return book.orElseThrow(() -> new BookNotFoundException("Book id " + id + " not found"));
	}

}
