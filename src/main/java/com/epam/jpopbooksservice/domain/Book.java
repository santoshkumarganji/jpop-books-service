package com.epam.jpopbooksservice.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "book")
public class Book implements Serializable {

	private transient static final long serialVersionUID = 5170478279781702722L;

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String author;

	public Book() {

	}

	public Book(String name, String author) {
		this.name = name;
		this.author = author;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}

	@Override
	public String toString() {
		return String.format("Book [id=%s, name=%s, author=%s]", id, name, author);
	}

}
