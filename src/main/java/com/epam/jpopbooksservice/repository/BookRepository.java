package com.epam.jpopbooksservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.jpopbooksservice.domain.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
