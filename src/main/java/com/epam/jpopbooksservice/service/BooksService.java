package com.epam.jpopbooksservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.jpopbooksservice.domain.Book;
import com.epam.jpopbooksservice.repository.BookRepository;

@Service
public class BooksService {

	@Autowired
	private BookRepository bookRepository;

	public Book save(Book book) {
		return bookRepository.save(book);
	}

	public void remove(Long bookId) {
		bookRepository.deleteById(bookId);
	}

	public Optional<Book> getById(Long bookId) {
		return bookRepository.findById(bookId);
	}

	public List<Book> getAll() {
		return bookRepository.findAll();
	}

}
