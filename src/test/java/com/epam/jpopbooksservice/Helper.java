package com.epam.jpopbooksservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.epam.jpopbooksservice.domain.Book;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Helper {

	public static void setUp(List<Book> books) {
		Book firstBook = new Book("You are the Password to my Life", "Sudeep Nagarkar");
		Book secondBook = new Book("Half Girlfriend", "Chetan Bhagat");
		Book thirdBook = new Book("Steve Jobs", "Walter Isaacson");
		Book fourthBook = new Book("Hit Refresh", "Satya Nadella");
		firstBook.setId(101L);
		secondBook.setId(102L);
		thirdBook.setId(103L);
		fourthBook.setId(104L); 
		books.add(firstBook);
		books.add(secondBook);
		books.add(thirdBook);
		books.add(fourthBook);
	}

	public static String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	public static <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

}
