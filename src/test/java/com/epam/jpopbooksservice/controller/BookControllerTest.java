package com.epam.jpopbooksservice.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.jpopbooksservice.Helper;
import com.epam.jpopbooksservice.domain.Book;
import com.epam.jpopbooksservice.service.BooksService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private BooksService bookService;

	@BeforeEach
	void setUp() {
		Long bookId = 101L;
		List<Book> books = new ArrayList();
		Helper.setUp(books);
		given(bookService.getAll()).willReturn(books);
		given(bookService.getById(bookId)).willReturn(books.stream().filter(book -> bookId == book.getId()).findAny());
	}

	@Test
	@DisplayName("Getting all books")
	void getAll() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/books").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andReturn();
		String responseContent = mvcResult.getResponse().getContentAsString();
		Book[] responseBooks = Helper.mapFromJson(responseContent, Book[].class);
		assertThat(bookService.getAll().size(), is(equalTo(responseBooks.length)));
	}

	@Test
	@DisplayName("Getting book by valid book id")
	void getById() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/books/101").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andReturn();
		String responseContent = mvcResult.getResponse().getContentAsString();
		Book responseBooks = Helper.mapFromJson(responseContent, Book.class);
		assertThat(bookService.getById(101L).get().getName(), is(equalTo(responseBooks.getName())));
	}

	@Test
	@DisplayName("Getting book by invalid book id")
	void getByInvalidId() throws Exception {
		mockMvc.perform(get("/books/10112546").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isNotFound()).andReturn();
	}

	@Test
	@DisplayName("Adding a new book")
	void add() throws Exception {
		Book mockBook = new Book("You are the Password to my Life", "Sudeep Nagarkar");
		given(bookService.save(Mockito.any(Book.class))).willReturn(mockBook);
		mockMvc.perform(post("/books").accept(MediaType.APPLICATION_JSON_VALUE).content(Helper.mapToJson(mockBook))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andReturn();
	}

	@Test
	@DisplayName("Removing a book")
	void remove() throws Exception {
		mockMvc.perform(delete("/books/101")).andExpect(status().isNoContent());
	}

	@Test
	@DisplayName("Update a book")
	void update() throws Exception {
		Book mockBook = new Book("Half Girlfriend", "Chetan Bhagat");
		mockMvc.perform(put("/books/101").accept(MediaType.APPLICATION_JSON_VALUE).content(Helper.mapToJson(mockBook))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(204)).andReturn();
	}
}
