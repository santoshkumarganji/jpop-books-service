package com.epam.jpopbooksservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.jpopbooksservice.domain.Book;
import com.epam.jpopbooksservice.repository.BookRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class BookServiceTest {

	@Autowired
	private BookRepository bookService;

	@Test
	@DisplayName("add book")
	void save() {
		Book book = new Book("You are the Password to my Life", "Sudeep Nagarkar");
		Book savedBook = bookService.save(book);
		assertEquals(book.getName(), savedBook.getName());
	}

	@Test
	@DisplayName("remove book")
	void remove() {
		int noOfBooks = bookService.findAll().size();
		save();
		assertEquals(noOfBooks + 1, bookService.findAll().size());
		bookService.deleteById(101L);
		assertEquals(noOfBooks, bookService.findAll().size());
	}

	@Test
	@DisplayName("getting book by book id")
	void getById() {
		Book book = new Book("You are the Password to my Life", "Sudeep Nagarkar");
		book.setId(101L);
		Book savedBook = bookService.save(book);
		assertEquals(book.getName(), bookService.findById(101L).get().getName());
	}

	@Test
	@DisplayName("getting books")
	void getAll() {
		assertEquals(4, bookService.findAll().size());

	}

}
